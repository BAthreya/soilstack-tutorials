# Add Field
The field drawing tool is on the app and can be reached at [https://app.soilstack.io/field/new](https://app.soilstack.io/field/new). It is best to use this tool on a computer


![Select from menu](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/select_from_menu.png)

Add in details about your field. The only required fields on the form are "Name" and "Group". The "Group" option will be based on the groups you are a member of, and other admins of that group will have access to the new field. Including “Producer Name” will allow you to sort the fields by that name in the app when you are sampling. Including “Address” and “Contact” it will allow you to navigate to the farm address and make a call to the producer in the app.  

![Enter field details](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/enter_field_details.png)

![Enter field details 2](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/enter_field_details2.png)

When you are drawing the field, you can navigate to the general area by putting in the address in the upper right corner search bar. Then start with “+ Boundary” and draw the outline of the field. If you need to include another polygon, you can click the “+ Boundary” again after you have accepted the original boundary to draw a second polygon. If you need to remove an internal portion of the field, you can use the “+ Cutout” button and draw out the area you wish to exclude from the field, similar to how you drew the first boundary. 

![Draw field boundary](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/draw_field_boundary.png)

When you are happy with the boundary, you can click “NEXT” in the lower right corner to get to the “Review” stage which will show you how the field will look in the app that you can double check before submitting.

![Review field](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/review.png)
