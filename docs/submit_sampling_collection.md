# Submitting a Sampling Collection

The sampling metadata collected in the app is stored locally on your device until it is submitted to the database. It is best to wait until the sampling collection is complete before electing to `SUBMIT` the sampling collection metadata.

![Submit](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Submitting/submitting_sampling_collection.png)

### Data submitted with a Sampling Collection

- Label ID
- Sample ID
- Sampling ID (Location)
- Sampling Collection ID (Field + time)
- Depth range of sample
- Actual GPS waypoints
- Accuracy of GPS at time of collection
- Intended GPS location of sampling
- Timestamp 

### Exporting a Lab Inventory

In most cases, the soils collected using SoilStack need to be shipped to a lab for analysis. After analysis is complete, the lab results will need to be merged with the sampling collection metadata. SoilStack provides a "LAB INVENTORY" export that will help to track sample data through this process.

- Once you have Submitted a sampling collection, you can select “LAB INVENTORY”.
- Or, from the Sampling Collections Tab you can check multiple Sampling Collections and then select “LAB INVENTORY”
- This will download a csv of the sample inventory to your device

***Note**: Email the downloaded lab inventory to the lab that is analyzing the samples*

![Exporting lab inventory](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Submitting/exporting_lab_inventory.png)

### What’s in the Lab Inventory

- Label ID
- Sample ID
- Sampling ID (Location)
- Sampling Collection ID (Field + time)
- Depth range of sample

If the lab returns results that keep the “Sample ID”, “Sampling ID” and “Sampling Collection ID” we can automate merging of lab results with sampling metadata


