# Before Sampling

## Soilstack Terminology
| Term | Definition |
| ---  | ---        |
| Sampling collection | Collecting soil samples from all the sampling locations for the entire field. |
| Sampling location | Every location in that field where you put the soil probe in the ground. |
| Sampling | Collecting all of the soil samples from that sampling location <br> (could be 1 or more samples). |
| Sample | individual soil sample (ex: if you are collecting multiple depth <br> increments at a sampling location each depth increment is a sample)|

## Quick Checklist Before Heading to Field  
- Accept the “SoilStack Invitation”
- “Install” the app to the Homescreen.
- QR codes generated and printed
     - QR Codes do not need to be producer specific or link to specific locations in the field. They only need to be unique within the project.
    - If you are collecting regular soil cores and separate bulk density cores, you can create a separate set of QR codes with a ‘bd’ prefix or suffix to distinguish between the sample types.
- Set field to “offline” if you anticipate poor connectivity (see below)


## Improving Offline Functionality

- If you anticipate not having internet access in the field, you can improve the apps performance by selecting “offline” in advance.
    - You must be connected to the internet when you switch to offline
    - This will download the base layers so you can navigate better in the field
    - The app will still function offline, even if the "offline" toggle is off, but the map layers may not be visible

![Toggle offline](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/online_offline.png)

