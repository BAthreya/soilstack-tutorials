# Get Started in the App

## Fields (Landing) Page
The Fields page is the primary landing page for the app and will display all of the fields that you have access to. You can use the search and filter functions to find fields.

***Note:** It may take a few minutes for all of your fields to load.*

![landing page.jpg](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/SoilStack_landing_page.png)


## Searching for Fields  
You can use the search, filter, and map features to find fields based on name, status, group or location.

### Desktop

![Filter fields](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Search%20Fields/desktop/filter.png)

![Select from map](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Search%20Fields/desktop/select_from_map.png)

### Phone

![Search and filter](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Search%20Fields/phone/search_and_filter.png)

## Export Field Information
Export a csv of field information based on the settings in the `Fields List`

- Select the menu in the upper right of the page (3 vertical dots)
- Select `Export csv`
- The csv download will include:
    - Field name
    - Producer name
    - Group
    - Status
    - Field centroid lat/long
    - Contact phone number
    - Contact Email

![export_fields_list.png](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/export_fields_list.png)


## Field Information Page
The field information page provides information about the producer, location and stratification and sampling collection history. From this page you can:

- Edit field and producer information
- Get directions to the field
- Get directions to the producers address
- Start or continue sampling collections
- View and edit completed sampling collections

![Field Information Page](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Field_Information_Page.png)


## Menu Options
The hamburger menu in the top left corner will allow you to access different parts of the app.
![Hamburger menu](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Hamburger%20Menu/hamburger%20menu.png)

- **Add Field**: Draw new fields directly in SoilStack, more information available [here](https://our-sci.gitlab.io/software/soilstack-tutorials/add_field/)

- **Sampling Collections**: View in-progress and completed sampling collections. More information availabe [here](https://our-sci.gitlab.io/software/soilstack-tutorials/return_to_sampling_collections/)

- **Admin**: Manage access to fields from this page
![Admin](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Hamburger%20Menu/reassign.png)

- **Settings**: Select units and choose to show or hide location numbers on the map
![Settings](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Hamburger%20Menu/settings.png)

