# Collecting Soil Samples

## Get directions to the field
- Select a field
- Under "Address" click either:
    - "Navigate to Contact Address" to navigate to the contact address on file
    - "Navigate to Field Location" to navigate to the centroid of the field

![Getting to field](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/select%20field.JPG)

## Setting up the sampling collection
- Select the field
- Push the "COLLECT" button under the map of the field 

![Collect](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/start_collection.png)

- Enter the depth increments for the soil samples that you will be collecting. 
    - Enter the minimum and maximum depth for the 1st depth increment you are collecting.
    - Select “Add depth” and enter the min and max depths for the next depth increment.
    - For example, one depth of 0-30 cm or multiple increments such as 0-15 cm, 15-30 cm, etc
- Select “start” to begin sampling collection.

***Note:** The depth ranges you set here will be available for each sample in the dropdown menu for the entirety of the sampling collection!!*

![Depths](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/sample_depths.JPG)


## Navigate to each sampling location in the Field using GPS

- Click the icon in the upper right corner of the map to active your location
- “Allow” the app to use your device’s location

![Navigate to field](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/navigate_to_point.png)

- Select a point to navigate to. When moving, the app will tell you how far away you are from the selected point and how accurate the device’s location is.

![Select point](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/select_point.jpg)

### Troubleshooting Navigation Issues

If you cannot see your location on the map, and the icon in the upper right is orange, it means that you are not within the frame of the map. Once you move closer to a sample point your location should appear.

![Navigation issues](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/navigation_issues.jpg)

## Collecting a sample

- When you get to a collection location select “Collect Sampling”
- The “Min Depth” and “Max Depth” show the full range of depths entered when setting up your plan. 
- Select the “+” icon to add a sample.

***Note:** The timestamp and GPS location are captured when you select “COLLECT SAMPLING", but are not recorded until you press “SAVE”*

![Arriving at a point](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/arriving_at_point.png)

### Capturing Bag ID QR Code

- Select the QR code button 
- Allow the app to use your camera
- Scan the QR code on your sample bag

![QR Code](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/QR_code.png)

### Selecting Depth of sample

- Select the depth from the dropdown menu
- Select "Add"
- By selecting Custom you can enter in any depth increment

![Select Depth](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/select_depth.png)

## Updating GPS Location When Changing Location

What if I cannot sample at the location?

Some sampling protocols allow the soil sampler to move up to 10 meters away from the sampling location if they cannot collect a sample for some reason. (Ex: rocky or compacted layer, in/near standing water, in driving lane in the field, etc)

The App captures the GPS location when you select COLLECT SAMPLING. If you need to update the location to a new sampling location:

- Press the target icon next to the GPS location to manually update the location OR
- CANCEL out  of the sampling collection page and re-enter the page

![Update GPS](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/update_gps.png)

## Check Progress

- Select the progress menu in the upper left corner
- See each sample collected at each sampling location
- Your progress is automatically saved, so you can leave and come back!

![Check Progress](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Collecting%20Samples/check_progress.png)


