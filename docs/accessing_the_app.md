# Accessing the app

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script> -->

If you are a part of a group, you need to log in to see your group's fields. If you are not a part of a group yet, ask the group admin to create an account for you. Brinda


## Accepting SoilStack invitations
Invitations are sent out by group administrators, and some groups may use *white-labelled* apps. So the invitation may take you to a SoilStack labelled app or to an app with branding specific to the group (similar to the example below).

1. You will receive an email from *teravestdan@gmail.com* with subject line “SoilStack Invite - `[Your Group]`. Click the link in the email to activate your invitation.

![Email Invitation](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Soilstack%20invitation/invitation_email.png)

2\. The link will take you to the invitation page.
     - If you already created a SoilStack account, select `LOGIN` and sign in using your existing username and password.
     - If you do not already have a SoilStack account, select `create account` and fill in your information to sign up.

![Link](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Soilstack%20invitation/accept_invitation.png)

![Create account](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Soilstack%20invitation/create_account.png)

3\. After logging in or creating your account, click `join [Your Group]` and you will be taken to that group's home screen and are able to access your fields.

![Join group](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Soilstack%20invitation/join.png)

## Installing SoilStack using Chrome

1. Go to app.soilstack.io, and select “Add SoilStack to Homescreen” from bottom 
2. If the option is not available, select the menu from the top right and select “Install App” 
3. Select “Install”
4. SoilStack will now be installed on your device and will work offline 

![Install with Chrome](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Installing%20soilstack/chrome_all.png)

## Installing SoilStack using Firefox 

1. Go to app.soilstack.io, and  select the menu from the  bottom right of the page 
2. Select “Install” 
3. Select “Add” 
4. SoilStack will now be installed on your device and will work offline 

![Install with Firefox](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Installing%20soilstack/firefox.png)

## Installing SoilStack using Safari 

1. Go to app.soilstack.io, and  select the options button on  the bottom of the page 
2. Select “Add to Home  Screen” 
3. Select “Add”
4. SoilStack will now be installed on your device and will work offline 

![Install with Safari](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Installing%20soilstack/safari_fox.png)


