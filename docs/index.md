# Welcome to SoilStack 
**SoilStack is an open source app designed to support smart agricultural and environmental sampling, capturing patterns of spatial variability and guiding users through in-field collection in a transparent, replicable, and user-friendly way.**

SoilStack is cross-platform, meaning that is works on **computers** and mobile devices, **android** and **iPhone**. It is a progressive web app, not a native app, which  means you do not go to the apple app store or google playstore to download the app. Instead, you go to app.soilstack.io and `Install` or `Add to Homescreen` depending on your device and browser type. Follow the links for more information on how to install the app in [Chrome](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#installing-soilstack-using-chrome), [Firefox](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#installing-soilstack-using-firefox), or [Safari](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#installing-soilstack-using-safari). Brinda


